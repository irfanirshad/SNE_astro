export const nav = [
	{
		title: 'Home',
		slug: '/',
	},
	{
		title: 'Blog',
		slug: '/blog',
	},
	{
		title: 'About',
		slug: '/company/about',
	},
	{
		title: 'Our Services',
		slug: '/company/about1',
	},
	{
		title: 'Contact',
		slug: '/company/contact',
	},
	{
		title: 'Legal',
		slug: '/company/legal',
	},
];
