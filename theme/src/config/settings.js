export default {
	title: `SN Enterprises | Chennai Real Estate Services`,
	description: `A simple, clean, and modern theme for a startup or businesses' marketing website.`,
	url: `https://odyssey-theme.littlesticks.dev`, // No trailing slash!
	name: `SNE`, // The short name of the business or brand name. Used for things like the copyright in the footer.
	enableThemeSwitcher: true,
	showLittleSticksPlug: false, // Disable this if you want to remove the Little Sticks plug from the footer. (╯°□°)╯︵ ┻━┻
};
